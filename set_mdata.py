from docx import Document
from sys import argv
import datetime
import shutil
from openpyxl import load_workbook
from openpyxl.packaging.custom import (
    BoolProperty,
    DateTimeProperty,
    FloatProperty,
    IntProperty,
    LinkProperty,
    StringProperty,
    CustomPropertyList,
)


def demand_value(mdata_title, def_val=""):
    ret_val = def_val
    istr = input("Enter value for '" +
                 mdata_title +
                 "' (default value = '" +
                 def_val +
                 "'):")
    if istr != "":
        ret_val = istr
    else:
        pass

    return ret_val


def format_err():
    print("Wrong format of the document file name. Exiting ...")
    exit(1)


if len(argv) < 2:
    print("python set_metadata.py docname [doc_owner]")
    print("where docname -> EN-AND-RS-DER-01-23-456_My_document.docx \
    OR EN-AND-RS-DER-01-23-456_My_document.xlsx")
    print("following the E-AND-PM-PLA-02-00-002 convention")
    print("doc_owner -> 'Fname LName'")
    exit(1)
else:
    if len(argv) > 2:
        doc_owner = argv[2]
    else:
        doc_owner = ""

fpath = argv[1]
if "/" in argv[1]:
    fn = argv[1].split("/")[-1]
else:
    fn = argv[1]

cdate = str(datetime.date.today())

if "_" in fn:
    doc_id = fn.split("_")[0]
else:
    format_err()

if "-" in fn:
    doc_type = doc_id.split("-")[3]
else:
    format_err()

title_start_index = len(doc_id) + 1

if "." in fn:
    title_end_index = fn.index(".")
else:
    format_err()

doc_title = fn[title_start_index:title_end_index].replace("_", " ")

sys_name = "ANDES" if "AND" in doc_id else "XXX"

doc_var_dict = {
    "DOCUMENT_version": "0.1",
    "DOCUMENT_status": "Draft",
    "DOCUMENT_creator": doc_owner,
    "DOCUMENT_author": doc_owner,
    "DOCUMENT_name": doc_title,
    "DOCUMENT_number": doc_id,
    "DOCUMENT_prep_date": cdate,
    "DOCUMENT_appr1_date": cdate.split("-")[0] + "-XX-XX",
    "DOCUMENT_type": doc_type,
    "DOCUMENT_releasedDate": cdate.split("-")[0] + "-XX-XX",
    "DOCUMENT_Title": doc_title,
    "DOCUMENT_rel_Organization": sys_name + " Consortium"
}

# no change props
nc_props = ["name",
            "number",
            "status",
            "type",
            "appr1",
            "releasedDate",
            "Title",
            "rel"]

if doc_owner != "":
    nc_props = nc_props + ["author", "creator"]

for k,v in doc_var_dict.items():
    if k.split("_")[1] not in nc_props:
        temp_v = demand_value(k,v)
        doc_var_dict[k] = temp_v

if ".docx" in fn:
    try:
        f = open(fn, "rb")
        f.close()
    except FileNotFoundError:
        shutil.copy('./ANDES_Documents_WordTemplate.docx', './' + fn)

    document = Document(fn)
    # fn = argv[1].split("\\")[-1]
    print(fn)
    # print "revision is: " % document.custom_properties["Revision"]

    for k, v in doc_var_dict.items():
        document.custom_properties[k] = v
    document.save(fn)
else:
    if ".xlsx" in fn:
        try:
            f = open(fpath, "rb")
            f.close()
        except FileNotFoundError:
            shutil.copy('./ANDES_Documents_ExcelTemplate.xlsx', './' + fn)

        wb = load_workbook(fn)
        ws = wb["CoverPage"]
        ws["D4"] = doc_title
        ws["D7"] = doc_id
        ws["D9"] = doc_type

        ws = wb["Authors & Change Record"]
        ws["C2"] = doc_owner
        ws["E2"] = cdate
        ws["B8"] = doc_owner

        # try setting props
        for k, v in doc_var_dict.items():
            try:
                wb.custom_doc_props.append(StringProperty(name=k, value=v))
            except ValueError:
                print("Prop ", k, " already exists")

        wb.save(fn)

    else:
        print("Unknown document file type. Exiting ...")
