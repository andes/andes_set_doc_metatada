# andes_set_doc_metatada



Small python script to create a word or excel document with the ANDES templates with most of the metadata set right.

usage : 

```
python set_metadata.py docname [doc_owner]
where docname -> EN-AND-RS-DER-01-23-456_My_document.docx OR
      	         EN-AND-RS-DER-01-23-456_My_document.xlsx
following the E-AND-PM-PLA-02-00-002 convention
doc_owner (optional) -> 'Fname LName'
```

Version and creation date will be asked in the terminal

If doc_owner is not given also creator(owner) and author names will be asked in the terminal

The script rewrites the metadata property values for an existing .docx with metadata defined, but will not do that for .xlsx files.

It requires a modified python-docx module that can be found here:

https://gitlab.unige.ch/andes/python-docx

You also need the template file that comes with the script


An experimental script to harvest a list of documents in the folder and all their metadata into an excel table:
python get_metadata.py
- reads word and excel files (.docx and .xlsx)
