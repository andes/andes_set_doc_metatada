from docx import Document
from openpyxl import Workbook, load_workbook
import os.path
import time
# from set_mdata import doc_var_dict


def get_flist(ext, path="."):
    # retrieve file list, filter by extension
    flist = os.listdir(path)
    ext_list = [ev for ev in flist if ext in ev]
    return ext_list


def get_mdata(fname):
    # retrieve meta data
    doc = Document(fname)
    mdata = doc.custom_properties.items()

    return mdata

def get_mdata_xlsx(fname):
    wb = load_workbook(fname)
    td = {}
    for prop in wb.custom_doc_props.props:
        td[prop.name] = prop.value

    return td
    

def get_fdata(fname):
    # retrieve file attributes data
    if os.path.isfile(fname):
        fst = os.stat(fname)
        fsize = fst.st_size
        fctime = fst.st_ctime
        retval = {"file_size": fsize, "file_ctime": fctime}
    else:
        retval = {"file_size": 0, "file_ctime": 0}
    return retval


def convert2array(mdata):
    # convert a dict structure to 2D array
    temp_arr = []
    header = []
    # find all different meta data titles
    for k, v in mdata.items():
        for h, t in v.items():
            if h not in header:
                header.append(h)
            else:
                pass

    # fill in the table
    header.sort()
    temp_arr.append(["file_name"] + header)
    for fn, md in mdata.items():
        print(fn)
        temp_arr.append([fn] + ["" for i in range(len(header))])
        print(temp_arr)
        for h, t in md.items():
            temp_arr[-1][header.index(h) + 1] = t

    return temp_arr


# prepare document list xlsx file
out_fname = "doc_list.xlsx"

if os.path.isfile(out_fname):
    wb = load_workbook(out_fname)
else:
    wb = Workbook()

ws = wb.create_sheet(time.strftime("%Y%m%d%H%M"))

# get list of docx documents

docx_list = get_flist(".docx")
xlsx_list = get_flist(".xlsx")
data = {}

for ev in docx_list:
    temp_data = get_mdata(ev)
    for k, v in get_fdata(ev).items():
        temp_data[k] = v
    data[ev] = temp_data

for ev in xlsx_list:
    temp_data = get_mdata_xlsx(ev)
    for k, v in get_fdata(ev).items():
        temp_data[k] = v
    data[ev] = temp_data

xls_arr = convert2array(data)
for ev in xls_arr:
    ws.append(ev)

# get list of xlsx documents
# xlsx_list = get_flist(".xlsx")

# close the doc list file
wb.save(out_fname)
